from flask_restplus import Api
from flask import Blueprint

from .main.controller.auth_controller import auth_api


blueprint = Blueprint('api', __name__)

api = Api(
    blueprint,
    title='Members API',
    version='0.1',
    description='REST API for managing organization members'
)

# add resource namespaces here
api.add_namespace(auth_api, path='/auth')
