from .. import db
import datetime


class AccessToken(db.Model):
    """
    AccessToken Model for storing JWT tokens associated with users
    """
    __tablename__ = 'access_token'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    token = db.Column(db.String(500), unique=True, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    created = db.Column(db.String(500), unique=True, nullable=False)
    expires = db.Column(db.DateTime, nullable=False)

    def __init__(self, token, user_id, created=datetime.datetime.utcnow(), expires=datetime.datetime.utcnow()):
        self.token = token
        self.user_id = user_id
        self.created = created
        self.expires = expires

    def __repr__(self):
        return '<id: token: {}'.format(self.token)

    def is_expired(self) -> bool:
        return self.expires <= datetime.datetime.utcnow()
