import datetime
import jwt
import uuid

from app.main.models.access_token import AccessToken

from ..config import key
from .. import db


class User(db.Model):
    """ User model for storing data related to registered users """
    __tablename__ = "user"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    email = db.Column(db.String(255), unique=True, nullable=False)
    created = db.Column(db.DateTime, nullable=False)
    is_admin = db.Column(db.Boolean, nullable=False, default=False)
    public_id = db.Column(db.String(100), unique=True)

    def __repr__(self):
        return "<User '{}'>".format(self.username)

    def generate_access_token(self) -> bytes:
        """
        Generates an Auth Token, taking user id as the subject claim
        """
        payload = {
            'exp': datetime.datetime.utcnow() + datetime.timedelta(days=1),
            'iat': datetime.datetime.utcnow(),
            'sub': self.user_id
        }
        token = jwt.encode(
            payload,
            key,
            algorithm='HS256'
        )
        return AccessToken(token=token, user_id=self.public_id)
