import uuid
from datetime import datetime
from typing import Dict

from app.main import db
from app.main.models.user import User


class UserService:
    def get_or_create_user(self, teamsnap_user: Dict) -> User:
        """
            Given data about the user from teamsnap, check if user is already registered
            If not, create a new user in the system
        """
        existing_user = User.query.filter_by(email=teamsnap_user['email']).first()
        if existing_user:
            return existing_user
        return self.__create_new_user(teamsnap_user)

    @staticmethod
    def __create_new_user(teamsnap_user):
        """Generate a user based on teamsnap fields and save to db, returning new user"""
        new_user = User(
            # generate unique user ID
            public_id=str(uuid.uuid4()),
            email=teamsnap_user['email'],
            created=datetime.utcnow()
        )
        db.session.add(new_user)
        db.session.commit()
        return new_user
