from app.main.models.access_token import AccessToken
from ..util.teamsnap_client import teamsnap
from .user_service import UserService
from app.main import db


class AuthService:
    @staticmethod
    def get_user_access_token(auth_code):
        """
        Takes a teamsnap auth code and fetches the user data from teamsnap. If
        a valid user is returned, an access token is generated and associated with the user,
        then returned in the response
        :param auth_code:
        :return: AccessToken
        """
        teamsnap_user = teamsnap.exchange_code_for_token(auth_code)
        if teamsnap_user:
            user = UserService.get_or_create_user(teamsnap_user)
            access_token = user.generate_access_token()
            return access_token
        raise(AuthenticationError('Could not authenticate with Teamsnap'))



    @staticmethod
    def logout_user(token: AccessToken):
        """TODO: implement logout, i.e. destroy token"""
        return

    @staticmethod
    def __save_access_token(token: AccessToken):
        """
        Commit newly created token to db
        """
        db.session.add(token)
        db.session.commit()

    @staticmethod
    def get_logged_in_user_from_request_headers(request):
        """TODO: Fetch user for provided access token"""
        return None


class AuthenticationError(Exception):
    def __init__(self, msg):
        self.msg = msg


class AuthorizationError(Exception):
    def __init__(self, msg):
        self.msg = msg
