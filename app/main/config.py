import os

# uncomment the line below for postgres database url from environment variable
# postgres_local_base = os.environ['DATABASE_URL']

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    SECRET_KEY = os.getenv('SECRET_KEY', 'my_secret_key')
    TEAMSNAP_CLIENT_ID =\
        os.getenv('TEAMSNAP_CLIENT_ID', '')
    TEAMSNAP_SECRET = os.getenv('TEAMSNAP_SECRET', '')
    # local testing URL as specified by teamsnap
    TEAMSNAP_REDIRECT_URI = os.getenv('TEAMSNAP_SECRET', 'urn:ietf:wg:oauth:2.0:oob')
    DEBUG = False


class DevelopmentConfig(Config):
    # uncomment the line below to use postgres
    # SQLALCHEMY_DATABASE_URI = postgres_local_base
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'members_app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class TestingConfig(Config):
    DEBUG = True
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'members_app_test.db')
    PRESERVE_CONTEXT_ON_EXCEPTION = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class ProductionConfig(Config):
    DEBUG = False
    # uncomment the line below to use postgres
    # SQLALCHEMY_DATABASE_URI = postgres_local_base


config_by_env = dict(
    dev=DevelopmentConfig,
    test=TestingConfig,
    prod=ProductionConfig
)

key = Config.SECRET_KEY

teamsnap_client_id = Config.TEAMSNAP_CLIENT_ID
teamsnap_secret = Config.TEAMSNAP_SECRET
teamsnap_redirect_uri = Config.TEAMSNAP_REDIRECT_URI
