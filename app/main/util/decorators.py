from functools import wraps
from flask import request

from app.main.service.auth_service import AuthService, AuthorizationError
from ..util.response_body import failure_response


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        try:
            AuthService.get_logged_in_user_from_request_headers(request)
        except AuthorizationError as e:
            return failure_response(403, e.msg)

        return f(*args, **kwargs)

    return decorated


def admin_token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):

        try:
            user = AuthService.get_logged_in_user_from_request_headers(request)
            is_admin = user.get('is_admin')
            if not is_admin:
                return failure_response(403, 'Not authorized')
        except AuthorizationError as e:
            return failure_response(403, e.msg)

        return f(*args, **kwargs)

    return decorated
