"""
Defines how models are parsed or serialized to JSON response
"""
from flask_restplus import Model, fields

token_serializer = Model('AccessToken', {
    'token': fields.String(required=True, description='Encoded access token'),
    'expires': fields.DateTime(required=True, description='Token expiry')
})
