from typing import Tuple, Dict


def failure_response(status_code: int, message: str) -> Tuple[Dict, int]:
    response_body = {
      'status': 'failure',
      'message': message
    }
    return response_body, status_code


def success_response(status_code: int, message: str) -> Tuple[Dict, int]:
    response_body = {
        'status': 'success',
        'message': message
    }
    return response_body, status_code
