"""
Composes operations from the third party oauth temasnap api
"""
import requests
from typing import Dict

from ..config import teamsnap_client_id, teamsnap_secret, teamsnap_redirect_uri

TEAMSNAP_AUTH_SERVER = 'https://auth.teamsnap.com/oauth'
TEAMSNAP_API_BASE = 'https://apiv3.teamsnap.com/v3/'


class TeamsnapClient:
    def __init__(self, client_id, secret, api_token=None):
        self.client_id = client_id
        self.secret = secret
        self.api_token = api_token

    def exchange_code_for_user(self, auth_code):
        """ Takes teamsnap auth code and exchanges it
        for a token if user is valid"""
        resp = requests.post(
            f'{TEAMSNAP_AUTH_SERVER}/token',
            params={
                'client_id': self.client_id,
                'client_secret': self.secret,
                'redirect_uri': teamsnap_redirect_uri,
                'code': auth_code,
                'grant_type': 'authorization_code'
            },
            headers={'Content-Length': 0}
        )
        resp.raise_for_status()
        body = resp.json()
        self.api_token = body['access_token']
        # TODO: eventually also check that user is member of this app's team ID
        return self.__fetch_teamsnap_user_data()

    def __fetch_teamsnap_user_data(self) -> Dict:
        """Uses teamsnap api token to fetch user data from teamsnap API"""
        resp = requests.get(f'{TEAMSNAP_API_BASE}/me', headers={'Authorization': f'Bearer {self.api_token}'})
        resp.raise_for_status()
        # teamsnap serializes data according to Collection+JSON
        # TODO come up with a better way to parse this API format
        body = resp.json()
        user_data = body['collection']['items'][0]
        # https://stackoverflow.com/questions/8653516/python-list-of-dictionaries-search
        email_datum = next(item for item in user_data if item["name"] == "email")
        return {
            'email': email_datum['value']
        }


teamsnap = TeamsnapClient(teamsnap_client_id, teamsnap_secret)
