"""
Handles HTTP requests and serialization of responses etc
Also handles routing inline
"""
from flask import request
from flask_restplus import Resource

from ..util.dto import UserDTO
from ..util.decorators import admin_token_required
from ..service.user_service import save_new_user, get_all_users, get_user

user_api = UserDTO.user_api
user_payload = UserDTO.user_payload


@user_api.route('/')
class AllUsers(Resource):

    @user_api.doc('Get list of all registered users')
    @user_api.marshal_list_with(user_payload, envelope='data')
    @admin_token_required
    def get(self):
        """List all registered users"""
        return get_all_users()

    @user_api.doc('Create a new user')
    # TODO: this action will not be publicly accessible after teamsnap auth is enabled
    @user_api.expect(user_payload, validate=True)
    @admin_token_required
    def post(self):
        """Creates a new User """
        data = request.json
        return save_new_user(data=data)


@user_api.route('/<string:public_id>')
@user_api.param('public_id', 'The User identifier')
class User(Resource):
    @user_api.doc('Get user by ID')
    @user_api.marshal_with(user_payload)
    @admin_token_required
    def get(self, public_id):
        """get a user given its identifier"""
        user = get_user(public_id)
        if not user:
            user_api.abort(404)
        else:
            return user
