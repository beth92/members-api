"""
Implement HTTP routes and actions for auth-related endpoints
"""
from flask import request
from flask_restplus import Resource, Namespace, marshal

from app.main.service.auth_service import AuthService
from ..util.serializers import token_serializer
from ..util.decorators import token_required
from ..util.response_body import failure_response, success_response

auth_api = Namespace('auth', description='Authentication related operations')
auth_api.models[token_serializer.name] = token_serializer


@auth_api.route('/login')
class UserLogin(Resource):
    """
        POST request to login new or existing user via
        provided auth code from third party.
        I.e. get an access token
    """
    @auth_api.doc('User login')
    def post(self):
        # get the code from params
        auth_code = request.args.get('code')
        if not auth_code:
            return failure_response(401, 'Unauthorized')
        access_token = AuthService.get_user_access_token(auth_code)
        return marshal(access_token, token_serializer), 201


@auth_api.route('/logout')
class LogoutAPI(Resource):
    """
    Logout Resource
    """
    @auth_api.doc('logout a user')
    @token_required
    def post(self):
        # get auth token
        auth_header = request.headers.get('Authorization')
        return AuthService.logout_user(data=auth_header)
