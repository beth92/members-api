# Basic Flask REST API

Useful scripts:

`source env/bin/activate` - activate virtualenv

`pip install -r requirements.txt` - install dependencies 

`python manage.py run` - run the dev server

`python manage.py test` - run detected unit tests

`python manage.py db init` - create migrations directory (only do this once)

`python manage.py db migrate --message 'details of migration` - create a DB migration

`python manage.py db upgrade` - apply outstanding migrations

`python manage.py db downgrade` - 'undo' migrations

Every time the models change, run the DB migrate and upgrade commands.

